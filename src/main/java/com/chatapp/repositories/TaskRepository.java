package com.chatapp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.chatapp.entities.Task;
import com.chatapp.entities.User;




public interface TaskRepository extends JpaRepository<Task, Long>{

	List<Task> findByUser(User user);

}
