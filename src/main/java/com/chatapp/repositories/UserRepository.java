package com.chatapp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.chatapp.entities.User;



public interface UserRepository extends JpaRepository<User, String> {

	User findOne(String email);

	List<User> findByNameLike(String name);

	

}
