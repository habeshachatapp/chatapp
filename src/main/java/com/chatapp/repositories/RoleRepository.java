package com.chatapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.chatapp.entities.Role;



 

public interface RoleRepository extends JpaRepository<Role, String>{


}
