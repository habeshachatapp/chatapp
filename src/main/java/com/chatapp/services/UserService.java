package com.chatapp.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.chatapp.entities.Role;
import com.chatapp.entities.User;
import com.chatapp.repositories.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;
	
	public void createUser(User user) {
		BCryptPasswordEncoder encode = new BCryptPasswordEncoder();
	
		user.setPassword(encode.encode(user.getPassword()));
		Role userRole = new Role("USER");
		List<Role> roles = new ArrayList<>();
		roles.add(userRole);
		user.setRoles(roles);
		userRepository.save(user);
	}
	public void createAdmin(User user) {
		BCryptPasswordEncoder encode = new BCryptPasswordEncoder();
		/**BCryptPasswordEncoder encoder = null;**/
		user.setPassword(encode.encode(user.getPassword()));
		Role userRole = new Role("ADMIN");
		List<Role> roles = new ArrayList<>();
		roles.add(userRole);
		user.setRoles(roles);
		userRepository.save(user);
	}
	public User findOne(String email) {
		
		return userRepository.findOne(email);
		
	}
	public boolean isUserPresent(String email) {
		User u=userRepository.findOne(email);
		if(u!=null)
			return true;
		return false;
	}
	public List<User> findAll() {
		// TODO Auto-generated method stub
		return userRepository.findAll();
	}
	public List<User> findByName(String name) {
		// TODO Auto-generated method stub
		return userRepository.findByNameLike("%"+name+"%");
	}
	
}
